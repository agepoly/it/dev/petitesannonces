from typing import List
import os
from os.path import abspath, dirname
import yaml

app_config = os.environ.get("APP_CONFIG", dirname(abspath(__file__)) + "/config.yml")

with open(app_config, 'r') as stream:
    cfg = yaml.safe_load(stream)

hosts = []  # type: List[str]

hosts_from_env = os.environ.get("HOST", "")
if hosts_from_env:
    hosts = hosts_from_env.split(":")
else:
    hosts = cfg["ALLOWED_HOSTS"]

if not hosts:
    raise Exception("Could not deternime Application Host.")

DATABASES = cfg['DATABASES']

# Make this unique, and don't share it with anybody.
SECRET_KEY = cfg['SECRET_KEY']

DEBUG = cfg['DEBUG']

ACTIVATE_RAVEN = False

ALLOWED_HOSTS = cfg['ALLOWED_HOSTS']

CACHES = cfg['CACHES']

SESSION_ENGINE = cfg['SESSION_ENGINE']
POLYCLASSIFIEDADS_RSS_SECRET = cfg['POLYCLASSIFIEDADS_RSS_SECRET']
