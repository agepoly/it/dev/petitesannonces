petiteannonces is a system to publish ads developped for AGEPOly.
The django package and documentation: https://github.com/PolyLAN/polyclassifiedads

This repo aim to dockerize petitesannonce.

Important note:

The docker file is workking and can bootstrap the app but it doesn't work as expected.
All the issue related to the containairization: https://gitlab.com/agepoly/it/dev/petitesannonces/issues

How to run and debug:

1) Clone this repo

2) Build the docker image

    docker build . -t petitesannonces

3) Run the image

    docker container run -p 80:80 -p 443:443 petitesannonces

4) Go inside the container, start debug the django app

    docker exec -it xxxxx /bin/bash
